#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-syncthing-lib.pl';
&ReadParse();

my @errors;

if (@errors) {
    &error(join('<br/>', @errors));
}

my $d;
if ($in{'dom'}) {
    $d = &virtual_server::get_domain($in{'dom'});
}

if (!$d) {
    push(@errors, $text{'syncthun_domain_missing'});
}

# Page title, must be first UI thing
&ui_print_header(
    'at ' . '<a href="https://' . $d->{'dom'} . '" target="_blank">https://' . $d->{'dom'} . '</a>',
    $text{'syncthing_stop_instance_header'}, "", undef, 1, 1
);

&$virtual_server::first_print($text{'syncthing_stop_instance'});

print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&syncthing_stop_instance_server($d, \%in);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&ui_print_footer(
    $d ? &virtual_server::domain_footer_link($d) : (),
    "index.cgi?dom=$in{'dom'}",
    $text{'index_the_information_page'}
);