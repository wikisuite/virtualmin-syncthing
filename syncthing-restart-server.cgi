#!/usr/bin/perl
use strict;
use warnings;

our (%access, %text, %in);
our $module_name;

require './virtualmin-syncthing-lib.pl';
&ReadParse();

my @errors;

if (@errors) {
    &error(join('<br/>', @errors));
}
my $d;
if ($in{'dom'}) {
    $d = &virtual_server::get_domain($in{'dom'});
}

if (!$d) {
    push(@errors, $text{''});
}

my $instance_status = &syncthing_is_running($d);
# Page title, must be first UI thing
&ui_print_header(
    'at ' . '<a href="https://' . $d->{'dom'} . '" target="_blank">https://' . $d->{'dom'} . '</a>',
    $instance_status ? $text{'syncthing_restart_instance_header'} : $text{'syncthing_start_instance_header'}, "", undef, 1, 1
);


&$virtual_server::first_print($instance_status ? $text{'syncthing_restart_instance'} : $text{'syncthing_start_instance'});

print '<pre style="white-space: pre-wrap; background-color: black; color: white;">' . "\n";
&syncyhing_start_instance_server($d, \%in);
print '</pre>' . "\n";
&$virtual_server::second_print(".. done");

&ui_print_footer(
    $d ? &virtual_server::domain_footer_link($d) : (),
    "index.cgi?dom=$in{'dom'}",
    $text{'index_the_information_page'}
);