use strict;
use warnings;
our (%text);
our $module_name;

do 'virtualmin-syncthing-lib.pl';
my $input_name = $module_name;
$input_name =~ s/[^A-Za-z0-9]/_/g;

sub feature_name {
    return $text{'feat_name'};
}

sub feature_label {
    return $text{'feat_name'};
}

sub feature_disname {
    return "My plugin will be temporarily de-activated";
}

sub feature_hlink {
    return "label";
}

sub feature_check {
    # check syncthing script is installed
    return undef;;

}
sub feature_suitable {
    my ($parent, $alias, $super) = @_;
    return $alias ? 0 : 1;
}

sub feature_enable {
    my ($d) = @_;
    &$virtual_server::first_print("Plugin in enable ..");
    &$virtual_server::second_print(".. done");
}

sub feature_losing {
    return "My plugin for this virtual server will be disabled";
}

sub feature_clash {
    return undef;
}

sub feature_depends {
    my ($d) = @_;
    if ($d->{'web'}) {
        return undef;
    }
    else {
        return "My plugin requires a website";
    }
}

sub feature_setup {
    my ($d) = @_;
    &$virtual_server::first_print("Setting up My plugin..");
    return 1;
}

sub feature_delete {
    my ($d) = @_;
    &$virtual_server::first_print("Turning off up My plugin..");
    # system("somecommand --remove $d->{'dom'} >/dev/null 2>&1");
    &$virtual_server::second_print(".. done");
}

sub feature_disable {
    my ($d) = @_;
    &$virtual_server::first_print("Removing $d->{'dom'} from  My plugin ..");
    &$virtual_server::second_print(".. done");
}

sub feature_validate {
    my ($d) = @_;
    my $file = "$d->{'home'}/.config/syncthing/config.xml";
    if (! -f $file) {
        return $text{'feat_missing_config_file'};
    }
    else {
        return undef;
    }
}

sub feature_links {
    my ($d) = @_;
    return ({
        'mod'  => $module_name,
        'desc' => "Syncthing",
        'page' => 'index.cgi?dom=' . $d->{'id'},
        'cat'  => 'services',
    });
}

sub feature_webmin {
    my ($d, $all) = @_;
    my @fdoms = grep {$_->{$module_name}} @$all;

    if (@fdoms) {
        return ([ $module_name, { 'doms' => join(" ", @fdoms) } ]);
    }
    else {
        return ();
    }
}

sub settings_links {
    return ({
        'title' => 'Syncthing',
        'link' => "/$module_name/index.cgi",
        'cat'  => 'services',
    });
}

sub feature_modules
{
    return ( [ $module_name, $text{'feat_module'} ] );
}