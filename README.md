# virtualmin-syncthing



## Getting started

Syncthing is a continuous file synchronization program.
It synchronizes files between two or more computers in real time, safely protected from prying eyes.
Then the plugins will allow to have the information of the instance which is installed on the virtual server. 
You can also update the user's credential, stop or restart the syncthing instance.
